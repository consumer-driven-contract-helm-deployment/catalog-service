require 'httparty'
require_relative 'models/t_shirt'

module CatalogApp
  class StockClient
    
    include HTTParty
    base_uri 'http://stock'

    def self.find_all_t_shirts
      response = get("/t-shirt", :headers => {'Accept' => 'application/json'})
      t_shirt_list = []
      when_successful(response) do
        body = parse_body(response)
        body.each do |t_shirt|
          t_shirt_list.push(CatalogApp::Products::TShirt.new(t_shirt))
        end
        t_shirt_list
      end
      t_shirt_list
    end

    def self.find_t_shirt_by_name name
      response = get("/t-shirt/#{name}", :headers => {'Accept' => 'application/json'})
      when_successful(response) do
        CatalogApp::Products::TShirt.new(parse_body(response))
      end
    end

    def self.when_successful response
      if response.success?
        yield
      elsif response.code == 404
        nil
      else
        raise response.body
      end
    end

    def self.parse_body response
      JSON.parse(response.body, {:symbolize_names => true})
    end

  end
end