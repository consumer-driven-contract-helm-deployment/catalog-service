require 'sinatra'
require_relative './stock_client'

configure do
  set :views, "#{File.dirname(__FILE__)}/views"
  set :show_exceptions, :after_handler
end

get '/' do
  @t_shirts = CatalogApp::StockClient.find_all_t_shirts
  erb :home
end

get '/product/t-shirt/:name' do
  @t_shirt = CatalogApp::StockClient.find_t_shirt_by_name params['name']
  erb :t_shirt
end
